// Dependencies
import webpack from 'webpack';
import path from 'path';
//import ChunksPlugin from 'webpack-split-chunks';

//paths
const PATHS = {
    index: path.join(__dirname, 'src/index'),
    build: path.join(__dirname, '/dist'),
    base: path.join(__dirname, 'src')
}

//webpack config
export default{
    devtool: getDevtool(),
    entry: getEntry(),
    output: getOutput(),
    plugins: getPlugins(),
    module: getLoaders()
}