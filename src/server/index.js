//Dependencies
import express from 'express'
import webpack from 'webpack'
import path from 'path'
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import open from 'open';
//import exphbs from 'express-handlebars';

// Webpack Configuration
import webpackConfig from '../../webpack.config.babel';

// API
//import blogApi from './api/blog';
//import libraryApi from './api/library';

const port = 3000

const app = express()

const webpackCompiler = webpack(webpackConfig)

//webpack middleware
app.use(webpackDevMiddleware(webpackCompiler));
app.use(webpackHotMiddleware(webpackCompiler));

//Sending all traffic to React
app.get('*', (req, res)=>{
    res.sendfile(path.join(__dirname, '../public/index.html'))
})

//listeng port 
app.listen(port, err =>{
    if(!err){
        open(`http//localhost:${port}`)
    }
})